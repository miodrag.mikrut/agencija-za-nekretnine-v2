package com.miodrag.mikrut.agencijazanekretnine.adapters;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.miodrag.mikrut.agencijazanekretnine.R;
import com.miodrag.mikrut.agencijazanekretnine.db.model.Slika;
import com.miodrag.mikrut.agencijazanekretnine.tools.Tools;

import java.util.List;

public class RVadapterListaSlika extends RecyclerView.Adapter<RVadapterListaSlika.MyViewHolder> {

    private List<Slika> rv_lista;

    public OnRecyclerItemClickListener listener;
    public interface OnRecyclerItemClickListener {
        void onRVItemClick(Slika slika);
    }

    public RVadapterListaSlika(List<Slika> rv_lista,OnRecyclerItemClickListener listener) {
        this.rv_lista = rv_lista;
        this.listener = listener;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;

            imageView = itemView.findViewById(R.id.rv_single_item_slika_imageview);
        }

        public void bind(final Slika objekat, final OnRecyclerItemClickListener listener) {
//            category.setText(objekat.getPhone_category());
                imageView.setImageBitmap(BitmapFactory.decodeFile(objekat.getImage_path()));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(objekat);
                }
            });
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_single_item_slika,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(rv_lista.get(i),listener);

    }

    @Override
    public int getItemCount() {
        return rv_lista.size();
    }

    //dodavanje i refresh rv liste
    public void setNewData(List<Slika> listaGrupa){
        this.rv_lista.clear();
        this.rv_lista.addAll(listaGrupa);
        notifyDataSetChanged();
    }

}
