package com.miodrag.mikrut.agencijazanekretnine.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "nekretnine")
public class Nekretnina {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "naziv")
    private String naziv;
    @DatabaseField(columnName = "opis")
    private String opis;
    @DatabaseField(columnName = "adresa")
    private String adresa;
    @DatabaseField(columnName = "telefon")
    private String telefon;
    @DatabaseField(columnName = "kvadratura")
    private String kvadratura;
    @DatabaseField(columnName = "broj_soba")
    private String broj_soba;
    @DatabaseField(columnName = "cena")
    private String cena;
    @ForeignCollectionField(foreignFieldName = "nekretnina",eager = true)
    private ForeignCollection<Slika> slike;

    public Nekretnina() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getKvadratura() {
        return kvadratura;
    }

    public void setKvadratura(String kvadratura) {
        this.kvadratura = kvadratura;
    }

    public String getBroj_soba() {
        return broj_soba;
    }

    public void setBroj_soba(String broj_soba) {
        this.broj_soba = broj_soba;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return naziv;
    }
}
