package com.miodrag.mikrut.agencijazanekretnine.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.agencijazanekretnine.R;
import com.miodrag.mikrut.agencijazanekretnine.db.DatabaseHelper;
import com.miodrag.mikrut.agencijazanekretnine.db.model.Nekretnina;
import com.miodrag.mikrut.agencijazanekretnine.db.model.Slika;
import com.miodrag.mikrut.agencijazanekretnine.tools.Tools;

import java.sql.SQLException;

public class Edit extends AppCompatActivity {

    DatabaseHelper databaseHelper;

    private EditText naziv;
    private EditText opis;
    private EditText telefon;
    private EditText cena;
    private EditText kvadratura;
    private EditText adresa;
    private EditText broj_soba;
    private Button btn_save;
    private Button btn_cancel;
    private Nekretnina nekretnina;
    private int objekat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        inicijalizacija();
        preuzmiPodatkeIzIntenta();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelEdit();
            }
        });
    }

    private void inicijalizacija(){
        naziv = findViewById(R.id.edit_ev_naslov);
        opis = findViewById(R.id.edit_ev_opis);
        broj_soba = findViewById(R.id.edit_ev_broj_soba);
        kvadratura = findViewById(R.id.edit_ev_kvadratura);
        adresa = findViewById(R.id.edit_ev_adresa);
        telefon = findViewById(R.id.edit_ev_telefon);
        cena = findViewById(R.id.edit_ev_cena);
        btn_save = findViewById(R.id.edit_btn_save);
        btn_cancel = findViewById(R.id.edit_btn_cancel);

    }
    //prihvata podatak iz intenta i nalazi taj objekat.
    //njegovim podacima popunjava polja za editovanje
    private void preuzmiPodatkeIzIntenta(){
        objekat_id = getIntent().getIntExtra("objekat_id",-1);
        if(objekat_id < 0){
            Toast.makeText(this, "Greska! "+ objekat_id +" ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            nekretnina = getDatabaseHelper().getNekretninaDao().queryForId(objekat_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(Tools.proveraPrefsPodesavanja("toast",Edit.this)) {
            Toast.makeText(Edit.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
        }
        if(Tools.proveraPrefsPodesavanja("notifications",Edit.this)) {
            Tools.createNotification(this,nekretnina,"add");
        }

        naziv.setText(nekretnina.getNaziv());
        opis.setText(nekretnina.getOpis());
        telefon.setText(nekretnina.getTelefon());
        adresa.setText(nekretnina.getAdresa());
        kvadratura.setText(nekretnina.getKvadratura());
        cena.setText(nekretnina.getCena());
        broj_soba.setText(nekretnina.getBroj_soba());

    }
//proverava se da li je sve uneseno od polja i ako jeste setuje se objekat i upisuje u bazi
    private void saveChanges(){
        if(Tools.validateInput(naziv)
                && Tools.validateInput(opis)
                && Tools.validateInput(telefon)
                && Tools.validateInput(adresa)
                && Tools.validateInput(kvadratura)
                && Tools.validateInput(broj_soba)
                && Tools.validateInput(cena)

        ) {
            nekretnina.setNaziv(naziv.getText().toString());
            nekretnina.setOpis(opis.getText().toString());
            nekretnina.setTelefon(telefon.getText().toString());
            nekretnina.setAdresa(adresa.getText().toString());
            nekretnina.setKvadratura(kvadratura.getText().toString());
            nekretnina.setBroj_soba(broj_soba.getText().toString());
            nekretnina.setCena(cena.getText().toString());


            try {
                getDatabaseHelper().getNekretninaDao().createOrUpdate(nekretnina);
                if(Tools.proveraPrefsPodesavanja("toast",Edit.this)) {
                    Toast.makeText(Edit.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
                }
                if(Tools.proveraPrefsPodesavanja("notifications",Edit.this)) {
                    Tools.createNotification(this,nekretnina,"edit");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(Edit.this, Detail.class);
            intent.putExtra("objekat_id", nekretnina.getId());
            startActivity(intent);
        }
    }

    private void cancelEdit(){
        if(Tools.proveraPrefsPodesavanja("toast",Edit.this)) {
            Toast.makeText(Edit.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
        }
        if(Tools.proveraPrefsPodesavanja("notifications",Edit.this)) {
            Tools.createNotification(this,nekretnina,"cancel");
        }
        Intent intent = new Intent(Edit.this,Detail.class);
        intent.putExtra("objekat_id",nekretnina.getId());
        startActivity(intent);
    }
    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
