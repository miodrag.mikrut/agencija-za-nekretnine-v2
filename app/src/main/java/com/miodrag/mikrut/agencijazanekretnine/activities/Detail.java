package com.miodrag.mikrut.agencijazanekretnine.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.agencijazanekretnine.R;
import com.miodrag.mikrut.agencijazanekretnine.adapters.RVadapterListaSlika;
import com.miodrag.mikrut.agencijazanekretnine.db.DatabaseHelper;
import com.miodrag.mikrut.agencijazanekretnine.db.model.Nekretnina;
import com.miodrag.mikrut.agencijazanekretnine.db.model.Slika;
import com.miodrag.mikrut.agencijazanekretnine.tools.Tools;

import java.sql.SQLException;
import java.util.List;

public class Detail extends AppCompatActivity implements RVadapterListaSlika.OnRecyclerItemClickListener {

    private static final int SELECT_PICTURE = 1;
    public static final int notificationID = 111;

    DatabaseHelper databaseHelper;
    private ImageButton imageButton;
    private Button btn_add_image;
    private Button btn_razgledanje;
    private String image_path;
    private RecyclerView recyclerView;
    private RVadapterListaSlika rVadapterListaSlika;
    private Toolbar toolbar;

    private TextView naziv;
    private TextView opis;
    private TextView cena;
    private TextView kvadratura;
    private TextView telefon;
    private TextView adresa;
    private TextView broj_soba;
    private List<Slika> slike;
    private Nekretnina nekretnina;
    private int objekat_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setupToolbar();
        inicijalizacija();
        preuzmiPodatkeIzIntenta();
        setupRV();

        //odabirom slike iz galerije prikazuje se u imageButtonu
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();
            }
        });

        //dodaje se slika u bazu za id tog objekta
        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage();
            }
        });

        btn_razgledanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Tools.proveraPrefsPodesavanja("notifications",Detail.this)){
                    Tools.createNotification(Detail.this,nekretnina,"zakazi");
                }
            }
        });

    }

    //inicijalizacija polja
    private void inicijalizacija() {
        naziv = findViewById(R.id.detail_naziv);
        opis = findViewById(R.id.detail_opis);
        telefon = findViewById(R.id.detail_telefon);
        adresa = findViewById(R.id.detail_adresa);
        cena = findViewById(R.id.detail_cena);
        broj_soba = findViewById(R.id.detail_broj_soba);
        kvadratura = findViewById(R.id.detail_kvadratura);
        imageButton = findViewById(R.id.detail_image_button);
        btn_add_image = findViewById(R.id.detail_btn_add_image);
        btn_razgledanje = findViewById(R.id.detail_btn_zakazi_razgledanje);

    }
    //preuzima podatke koje se nalaze u intentu koji je trigerovao aktivnost
    //setuje vrednosti polja na trenutnu vrednost objekta izvucenog iz baze na osnovu objekat_id
    private void preuzmiPodatkeIzIntenta() {
        objekat_id = getIntent().getIntExtra("objekat_id", -1);
        if (objekat_id < 0) {
            Toast.makeText(this, "Greska! " + objekat_id + " ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            nekretnina = getDatabaseHelper().getNekretninaDao().queryForId(objekat_id);
            slike = getDatabaseHelper().getSlikaDao().queryForEq("nekretnina_id", objekat_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        naziv.setText(nekretnina.getNaziv());
        opis.setText(nekretnina.getOpis());
        telefon.setText(nekretnina.getTelefon());
        adresa.setText(nekretnina.getAdresa());
        cena.setText(nekretnina.getCena());
        broj_soba.setText(nekretnina.getBroj_soba());
        kvadratura.setText(nekretnina.getKvadratura());
        telefon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.dialPhoneNumber(nekretnina.getTelefon(), Detail.this);
            }
        });

    }

    //dodaje sliku u bazu za id tog objekta
    private void addImage(){
        try {
            //proverava da li je slika izabrana, ako ne onda ne upisuje se prazan objekat u bazu
            if (image_path != null) {
                Slika slika = new Slika();
                slika.setImage_path(image_path);
                slika.setNekretnina(nekretnina);
                getDatabaseHelper().getSlikaDao().createOrUpdate(slika);
                Toast.makeText(Detail.this, "Slika dodata", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(Detail.this, "Prvo izaberite sliku", Toast.LENGTH_SHORT).show();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rVadapterListaSlika.setNewData(getDatabaseHelper().getSlikaDao().queryForEq("nekretnina_id", nekretnina.getId()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //****  Pocetak setovanja toolbara
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;
            case R.id.action_create:
                Intent intent = new Intent(this, Add.class);
                startActivity(intent);
                setTitle("New phone");
                if (Tools.proveraPrefsPodesavanja("toast", Detail.this)) {
                    Toast.makeText(Detail.this, nekretnina + " added", Toast.LENGTH_SHORT).show();
                }
                if (Tools.proveraPrefsPodesavanja("notifications", Detail.this)) {
                    Tools.createNotification(Detail.this, nekretnina, "add");
                }
                break;
            case R.id.action_edit:
                Intent intent1 = new Intent(Detail.this, Edit.class);
                intent1.putExtra("objekat_id", nekretnina.getId());
                startActivity(intent1);
                break;
            case R.id.action_delete:
                delete();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //^^^ kraj setupa za toolbar

    //brise objekat i sve objekte koji su vezani
    //izbacuje alert dialog za potvrdu brisanja
    //prikazuje notifikacije ako je odabrana ta opcija u Settings
    private void delete() {
        AlertDialog dialogDelete = new AlertDialog.Builder(this)
                .setTitle("Delete group")
                .setMessage("Are you sure you want to permanently delete the group?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            List<Slika> slike = getDatabaseHelper().getSlikaDao().queryForEq("nekretnina_id", nekretnina.getId());

                            getDatabaseHelper().getNekretninaDao().delete(nekretnina);
                            for (Slika slika : slike) {
                                getDatabaseHelper().getSlikaDao().delete(slika);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if (Tools.proveraPrefsPodesavanja("toast", Detail.this)) {
                            Toast.makeText(Detail.this, nekretnina + " deleted.", Toast.LENGTH_SHORT).show();
                        }
                        if (Tools.proveraPrefsPodesavanja("notifications", Detail.this)) {
                            Tools.createNotification(Detail.this, nekretnina, "delete");
                        }
                        Intent intent = new Intent(Detail.this, Main.class);
                        startActivity(intent);
                        if (Tools.proveraPrefsPodesavanja("toast", Detail.this)) {
                            Toast.makeText(Detail.this, nekretnina + " added", Toast.LENGTH_SHORT).show();
                        }
                        if (Tools.proveraPrefsPodesavanja("notifications", Detail.this)) {
                            Tools.createNotification(Detail.this, nekretnina, "delete");
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }


    /**
     * Da bi dobili pristup Galeriji slika na uredjaju
     * moramo preko URI-ja pristupiti delu baze gde su smestene
     * slike uredjaja. Njima mozemo pristupiti koristeci sistemski
     * ContentProvider i koristeci URI images/* putanju
     * <p>
     * Posto biramo sliku potrebno je da pozovemo aktivnost koja icekuje rezultat
     * Kada dobijemo rezultat nazad prikazemo sliku i dobijemo njenu tacnu putanju
     */
    //**** pocetak za odabir slike
    //proverava dozvolu za biranje iz galerije
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    //ako je dozvoljeno bira se slika iz galerije
    private void selectPicture() {
        if (isStoragePermissionGranted()) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_PICTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    image_path = picturePath; //uzima imageButton path i stavlja ga da bude dostupno
                    cursor.close();
                    imageButton.setImageBitmap(BitmapFactory.decodeFile(image_path));
                    // String picturePath contains the path of selected Image
                }
            }
        }
    }
//*******kraj za odabir slike

    //inicijalizacija RV adatera
    private void setupRV() {
        recyclerView = findViewById(R.id.detail_rv_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        rVadapterListaSlika = new RVadapterListaSlika(slike, this);
        recyclerView.setAdapter(rVadapterListaSlika);
    }

    //interfejs RecyclerView-a koji na klik na item u RV listi salje intent sa parametrom ID kliknutog objekta
    @Override
    public void onRVItemClick(Slika slika) {
        Intent intent = new Intent(this, ImageZoom.class);
        intent.putExtra("objekat_id", slika.getId());

        startActivity(intent);
    }

    //***** za komuikaciju sa bazom i oslobadjanje resursa
    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
    //***** KRAJ za komuikaciju sa bazom i oslobadjanje resursa


}
