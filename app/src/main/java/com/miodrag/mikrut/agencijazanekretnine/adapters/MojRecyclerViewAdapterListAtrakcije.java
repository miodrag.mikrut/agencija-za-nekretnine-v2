package com.miodrag.mikrut.agencijazanekretnine.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.miodrag.mikrut.agencijazanekretnine.R;
import com.miodrag.mikrut.agencijazanekretnine.db.model.Nekretnina;

import java.util.List;

public class MojRecyclerViewAdapterListAtrakcije extends
        RecyclerView.Adapter<MojRecyclerViewAdapterListAtrakcije.MyViewHolder> {

    public List<Nekretnina> listNekretnina;
    public OnRecyclerItemClickListener listener;

    public interface OnRecyclerItemClickListener {
        void onRVItemClick(Nekretnina nekretnina);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvNaziv;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvNaziv = itemView.findViewById(R.id.tv_recycler_naziv);

        }

        public void bind(final Nekretnina objekat, final OnRecyclerItemClickListener listener) {
            tvNaziv.setText(objekat.getNaziv());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(objekat);
                }
            });
        }
    }

    public MojRecyclerViewAdapterListAtrakcije(List<Nekretnina> listNekretnina, OnRecyclerItemClickListener listener) {
        this.listNekretnina = listNekretnina;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_single_item, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listNekretnina.get(i), listener);

    }

    @Override
    public int getItemCount() {
        return listNekretnina.size();
    }


    public void add(Nekretnina kontakt) {
        listNekretnina.add(kontakt);
        notifyDataSetChanged();
    }
}
